let products = {
    point1: ["#", "./images/items/item1.jpg", "Analog Starter Kitte", "Preorder - Analog Starter Kit", "", "$98.00", "$79.00", "product1"],
    point2: ["#", "./images/items/item2.jpg", "Analog Cards", "Preorder - Analog Starter Kit", "(3-Pack)", "$39.00", "", "product2"],
    point3: ["#", "./images/items/item3.jpg", "Analog Travel Case", "Preorder - Analog Travel Case", "(Gray)", "$32.00", "", "product3"],
    point4: ["#", "./images/items/item4.jpg", "Analog Wood Card Holder", "Preorder - Analog Wood Card Holder", "", "$59.00", "", "product4"],
    point5: ["#", "./images/items/item5.jpg", "Modus Operandi - Artist Collab", "Modus Operandi - Artist Collab", "(3 Pack)", "$126.00", "$120.00", "product5"],
    point6: ["#", "./images/items/item6.jpg", "Modus Operandi 01 - Artist Collab", "Modus Operandi 01 - Artist Collab &nbsp;", "(White)", "$42.00", "", "product6"],
    point7: ["#", "./images/items/item7.jpg", "Modus Operandi 03 - Artist Collab", "Modus Operandi 03 - Artist Collab &nbsp; &nbsp;", "(Navy)", "$42.00", "", "product7"],
    point8: ["#", "./images/items/item8.jpg", "Modus Operandi 02 - Artist Collab", "Modus Operandi 02 - Artist Collab &nbsp;", "(Charcoal)", "$42.00", "", "product8"],
    point9: ["#", "./images/items/item9.jpg", "Kinto Travel Tumbler", "Kinto Travel Tumbler ", "(Black)", "$42.00", "", "product9"],
    point10: ["#", "./images/items/item10.jpg", "Circular", "Circular", "(Sienna)", "$36.00", "", "product10"],
    point11: ["#", "./images/items/item11.jpg", "Facet", "Facet", "(Black)", "$36.00", "", "product11"],
    point12: ["#", "./images/items/item12.jpg", "Rain", "Rain", "(Deep Forest)", "$36.00", "", "product12"],
    point13: ["#", "./images/items/item13.jpg", "Hollow", "Hollow", "(Port)", "$36.00", "", "product13"],
    point14: ["#", "./images/items/item14.jpg", "Face Mask", "Face Mask", "(Black)", "$10.00", "", "product14"],
    point15: ["#", "./images/items/item15.jpg", "Ripple Effect", "Ripple Effect", "(Charcoal Triblend)", "$36.00", "", "product15"],
    point16: ["#", "./images/items/item16.jpg", "Premium Leather Mousepad XL", "Premium Leather Mousepad XL", "(Ugmonk Logo - Natural)", "$52.00", "", "product16"],
    point17: ["#", "./images/items/item17.jpg", "Shoes Like Pottery", "Shoes Like Pottery", "(High Top - Natural)", "$165.00", "", "product17"],
    point18: ["#", "./images/items/item18.jpg", "Men's Essential Tee", "Men's Essential Tee", "(Starter Kit - 3-Pack)", "$96.00", "$89.00", "product18"],
    point19: ["#", "./images/items/item19.jpg", "Leather Coasters", "Leather Coasters", "(Enjoy the Journey - Set of 2 - Natural)", "$18.00", "", "product19"],
    point20: ["#", "./images/items/item20.jpg", "Gather Basic Set", "Gather Basic Set", "(Walnut)", "SOLD OUT", "", "product20"],
    point21: ["#", "./images/items/item20.jpg", "Gather Basic Set", "Gather Basic Set", "(Walnut)", "SOLD OUT", "", "product21", "./images/loader.gif", "loader"],
    point22: ["#", "./images/items/item12.jpg", "Rain", "Rain", "(Deep Forest)", "$36.00", "", "product22"],
};

var all = '';
for (let key in products) {  
    if (products[key][7] != 'product21' && products[key][7] != 'product22')
        all += `<div class="shop__photo-card">
                    <a class="link__product" href="${products[key][0]}" target="_blank">
                        <img class="shop__product-image" src="${products[key][1]}" alt="${products[key][2]}">
                        <div class="shop__product-info">
                            <h3 class="shop__product-title">${products[key][3]}</h3>
                            <p class="shop__product-describe">${products[key][4]}</p>
                        </div>
                        <p class="shop__product-price" id="${products[key][7]}">${products[key][5]}</p>
                        <p class="shop__product-price_new">${products[key][6]}</p>
                        
                    </a>
                </div>`;
    else if (products[key][7] == 'product21')
        all += `<div class="shop__photo-card">
                    <a class="link__product" href="${products[key][0]}" target="_blank">
                        <div class="shop__image-holder">
                            <img class="shop__loader" id="loader1" src="${products[key][8]}" alt="${products[key][9]}">
                            <img class="shop__product-image" src="${products[key][1]}" alt="${products[key][2]}">
                        </div>
                        <div class="shop__product-info">
                            <h3 class="shop__product-title">${products[key][3]}</h3>
                            <p class="shop__product-describe">${products[key][4]}</p>
                        </div>
                        <p class="shop__product-price" id="${products[key][7]}">${products[key][5]}</p>
                        <p class="shop__product-price_new">${products[key][6]}</p>
                        
                    </a>
                </div>`;
    else if (products[key][7] == 'product22')
        all += `<div class="shop__photo-card">
                    <a class="link__product" href="${products[key][0]}" target="_blank">
                        <div class="shop__image-holder">
                            <div class="shop__loader_another" id="loader2"></div>
                            <img class="shop__product-image" src="${products[key][1]}" alt="${products[key][2]}">
                            </div>
                        <div class="shop__product-info">
                            <h3 class="shop__product-title">${products[key][3]}</h3>
                            <p class="shop__product-describe">${products[key][4]}</p>
                        </div>
                        <p class="shop__product-price" id="${products[key][7]}">${products[key][5]}</p>
                        <p class="shop__product-price_new">${products[key][6]}</p>
                        
                    </a>
                </div>`;
}

document.getElementById('photoGrid').innerHTML = all;


for (let key in products) {
    if (products[key][6] != "") document.getElementById(products[key][7]).classList.add('shop__product-price_old');
}

let elem1 = document.getElementById('loader1');
let elem2 = document.getElementById('loader2');
setTimeout(function () {
elem1.remove();
elem2.remove();
}, 4000)
